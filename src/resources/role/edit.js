import React from 'react';
import { Edit, SimpleForm, TextInput } from 'react-admin'

function EditPage(props) {
  return (
    <Edit {...props}>
      <SimpleForm>
      <TextInput source="name"/>
      <TextInput source="description"/>
      </SimpleForm>
    </Edit>
  )
}

export default EditPage;