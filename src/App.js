import React, { Component } from 'react';
import { Admin, Resource } from 'react-admin';
import loopbackRestClient, { authClient } from 'aor-loopback';
import * as User from './resources/user';
import * as Role from './resources/role';

class App extends Component {
  render() {
    return (
      <Admin
        dataProvider={loopbackRestClient(`${process.env.REACT_APP_API_URL}/api`)}
        authProvider={authClient(`${process.env.REACT_APP_API_URL}/api/users/login`)}
      >
        {permissions => [
          <Resource
            name="users"
            list={permissions === 'admin' && User.List}
            edit={User.Edit}
            create={User.Create}
            show={User.Show}
            icon={User.Icon}
          />,
          <Resource
            name="roles"
            list={permissions === 'admin' && Role.List}
            edit={Role.Edit}
            create={Role.Create}
            show={Role.Show}
            icon={Role.Icon}
          />,
          <Resource name="rolemappings" />,
        ]}
      </Admin>
    );
  }
}

export default App;
