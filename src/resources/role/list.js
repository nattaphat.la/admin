import React from 'react';
import MyFilter from './filter';
import { List, Datagrid, TextField } from 'react-admin';

function ListPage(props) {
  return (
    <List {...props} filters={<MyFilter />}>
      <Datagrid>
        <TextField source="id" />
        <TextField source="name"/>
        <TextField source="description"/>
      </Datagrid>
    </List>
  )
}

export default ListPage;