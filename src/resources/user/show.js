import React from 'react';
import { Show, SimpleShowLayout, TextField, ReferenceField, ReferenceManyField, SingleFieldList, ChipField, DateField } from 'react-admin'
function ShowPage(props) {
  return (
    <Show {...props}>
      <SimpleShowLayout>
        <TextField source="id" />
        <ReferenceField label="Username" source="id" reference="users" linkType="show">
          <TextField source="username" />
        </ReferenceField>
        <ReferenceField label="Email" source="id" reference="users" linkType="show">
          <TextField source="email" />
        </ReferenceField>
        <ReferenceManyField label="Roles" reference="rolemappings" target="principalId">
          <SingleFieldList>
            <ReferenceField source="roleId" reference="roles" linkType="show">
              <ChipField source="name" />
            </ReferenceField>
          </SingleFieldList>
        </ReferenceManyField>
        <DateField source="createdAt" showTime />
        <DateField source="updatedAt" showTime />
      </SimpleShowLayout>
    </Show>
  )
}

export default ShowPage;