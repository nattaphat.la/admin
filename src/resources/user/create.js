import React from 'react';
import { Create, SimpleForm, TextInput, ReferenceInput, SelectInput } from 'react-admin';

function CreatePage(props) {
  return (
    <Create {...props}>
      <SimpleForm>
        <TextInput source="username"/>
        <TextInput source="email" type="email"/>
        <TextInput source="password" type="password"/>
        <ReferenceInput label="Role" source="role" reference="roles">
          <SelectInput optionText="name" />
        </ReferenceInput>
      </SimpleForm>
    </Create>
  )
}

export default CreatePage;