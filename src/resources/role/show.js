import React from 'react';
import { Show, SimpleShowLayout, TextField } from 'react-admin'
function ShowPage(props) {
  return (
    <Show {...props}>
      <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="name"/>
      <TextField source="description"/>
      </SimpleShowLayout>
    </Show>
  )
}

export default ShowPage;