import React from 'react';
import { Create, SimpleForm, TextInput } from 'react-admin';

function CreatePage(props) {
  return (
    <Create {...props}>
      <SimpleForm>
        <TextInput source="name"/>
        <TextInput source="description"/>
      </SimpleForm>
    </Create>
  )
}

export default CreatePage;