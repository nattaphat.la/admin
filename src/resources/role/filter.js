import React from 'react';
import { Filter, TextInput } from 'react-admin'
function MyFilter(props) {
  return (
    <Filter {...props}>
      <TextInput label="name" source="name" parse={v => ({ilike: `%${v}%`})} format={v => v && v.ilike.replace(/%/g, '')} />
    </Filter>
  )
}

export default MyFilter;