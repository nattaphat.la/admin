import React from 'react';
import { Filter, TextInput } from 'react-admin'
function MyFilter(props) {
  return (
    <Filter {...props}>
      <TextInput label="Username" source="username" parse={v => ({ilike: `%${v}%`})} format={v => v && v.ilike.replace(/%/g, '')} />
      <TextInput label="Email" source="email" parse={v => ({ilike: `%${v}%`})} format={v => v && v.ilike.replace(/%/g, '')} />
    </Filter>
  )
}

export default MyFilter;