import React from 'react';
import { Edit, SimpleForm, TextInput, ReferenceInput, SelectInput } from 'react-admin'

function EditPage(props) {
  return (
    <Edit {...props}>
      <SimpleForm>
        <TextInput source="username"/>
        <TextInput source="email" type="email"/>
        <ReferenceInput label="Role" source="role" reference="roles">
          <SelectInput optionText="name" />
        </ReferenceInput>
      </SimpleForm>
    </Edit>
  )
}

export default EditPage;