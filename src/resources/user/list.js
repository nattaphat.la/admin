import React from 'react';
import MyFilter from './filter';
import { List, Datagrid, TextField, ReferenceField, ReferenceManyField, SingleFieldList, ChipField, DateField } from 'react-admin';

function ListPage(props) {
  return (
    <List {...props} filters={<MyFilter />}>
      <Datagrid>
        <TextField source="id" />
        <ReferenceField label="Username" source="id" reference="users" linkType="show">
          <TextField source="username" />
        </ReferenceField>
        <ReferenceField label="Email" source="id" reference="users" linkType="show">
          <TextField source="email" />
        </ReferenceField>
        <ReferenceManyField label="Roles" reference="rolemappings" target="principalId">
          <SingleFieldList>
            <ReferenceField source="roleId" reference="roles" linkType="show">
              <ChipField source="name" />
            </ReferenceField>
          </SingleFieldList>
        </ReferenceManyField>
        <DateField source="createdAt" showTime />
        <DateField source="updatedAt" showTime />
      </Datagrid>
    </List>
  )
}

export default ListPage;